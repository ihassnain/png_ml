import cv2

cam=cv2.VideoCapture(0)
face=cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
lower_body=cv2.CascadeClassifier('haarcascade_lowerbody.xml')

while True:
    _,frame=cam.read()
    gray_frame=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)   
    face=full_body.detectMultiScale(image=gray_frame,scaleFactor=1.3)
    lwr_bdy=lower_body.detectMultiScale(image=gray_frame,scaleFactor=1.3)

    for (x,y,w,h) in face:
        cv2.putText(frame,'PPE_UNDETECTED',(40,40),cv2.FONT_HERSHEY_COMPLEX,2,(0,255,0))
        img = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
    for (x,y,w,h) in lwr_bdy:
        cv2.putText(frame,'LOWER',(70,40),cv2.FONT_HERSHEY_COMPLEX,2,(0,255,0))
        img = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
    cv2.imshow('detect',frame)
    if cv2.waitKey(1)==ord('q'):
        break


cam.release()
cv2.destroyAllWindows()
