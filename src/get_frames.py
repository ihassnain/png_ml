# Program To Read video 
# and Extract Frames 
import cv2 

def FrameCapture(path,path2): 

	vidObj = cv2.VideoCapture(path) 
	count = 0
	success = 1

	while success: 

		vidObj.set(cv2.CAP_PROP_POS_MSEC,(count*1000))  # Delay in millisecs for the frames 
		success, image = vidObj.read() 
		cv2.imwrite(path2 + "//room_images%d.jpg" % count, image) 

		count += 1

	print("Images collection Done")
if __name__ == '__main__': 

	img_dr = "/home/rospc/fast_ai/ML/png/videos/images/"   # path to directory where images to be saved 
	path_dr = "/home/rospc/fast_ai/ML/png/videos/IMG_5768.MOV"  # Path of video folder 

	FrameCapture(path_dr,img_dr)