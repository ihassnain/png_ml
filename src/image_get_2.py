# Program To Read video 
# and Extract Frames 
import cv2 

# Function to extract frames 
def FrameCapture(path): 
	
	vidObj = cv2.VideoCapture(path) 
	count = 0
	success = 1

	while success: 
        vidObj.set(cv2.cv.CAP_PROP_POS_MSEC,(count*2000))
		success, image = vidObj.read() 
		cv2.imwrite("frame%d.jpg" % count, image) 
		count += 1
if __name__ == '__main__': 

	# Calling the function 
	FrameCapture("/home/rospc/fast_ai/ML/png/videos/IMG_5769.MOV") 