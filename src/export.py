import fastbook
from fastbook import *

path = untar_data('https://dl.dropboxusercontent.com/s/dh1u3a3dfk7hjuz/png_final_1.zip')/'images'

def have_ppe(x): return x[0].isupper()

dls = ImageDataLoaders.from_name_func(path, get_image_files(path), valid_pct=0.2, seed=42,
    label_func=have_ppe, item_tfms=Resize(224))

model = cnn_learner(dls, resnet18, metrics=error_rate)
model.fine_tune(1)

model.export('/home/rospc/ppe_detector.pkl')