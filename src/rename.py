import os 
  
# Function to rename multiple files 
def main(): 
  
    for count, filename in enumerate(os.listdir("/home/rospc/fast_ai/ML/datasets/no_ppe/")): 
        dst ="no_ppe" + str(count) + ".jpg"
        src ='/home/rospc/fast_ai/ML/datasets/no_ppe/'+ filename 
        dst ='/home/rospc/fast_ai/ML/datasets/no_ppe/'+ dst 
          
        # rename() function will 
        # rename all the files 
        os.rename(src, dst) 
  
# Driver Code 
if __name__ == '__main__': 
      
    # Calling main() function 
    main() 
